<?php
include 'Model.php';
include 'Libro.php';
include 'Usuario.php';
$productos=new Libro();
$matrizProductos=$productos->all();

$usuarios=new Usuario();
$matrizUsuarios=$usuarios->all();
?>

 <!DOCTYPE html>
 <html lang="es" dir="ltr">
   <head>
     <meta charset="utf-8">
     <title>bookworld</title>
   </head>
   <body>
     <header>
       Catalogo dentro de la rama desarrollo
     </header>
     <table>
       <thead>
         <tr>
           <th>Id</th>
           <th>Nombre del libro</th>
           <th>Autor</th>
           <th>Editorial</th>
           <th>Genero</th>
           <th>Precio</th>
           <th>Descripción</th>
         </tr>
       </thead>
       <tbody>
         <?php
          foreach ($matrizProductos as $registro) {
            ?>
            <tr>
              <td><?php echo $registro['id']; ?></td>
              <td><?php echo $registro['nombre']; ?></td>
              <td><?php echo $registro['autor']; ?></td>
              <td><?php echo $registro['editorial']; ?></td>
              <td><?php echo $registro['genero']; ?></td>
              <td><?php echo $registro['precio']; ?></td>
              <td><?php echo $registro['descripcion']; ?></td>
              <?php
                echo '<td><a href="productoEliminar.php?idProducto='.$registro["id"].'">Eliminar</a></td>';
                echo '<td><a href="productoEditar.php?idProducto='.$registro["id"].'">Modificar</a></td>';
               ?>
            </tr>
            <?php
          }
          ?>
       </tbody>
     </table>
     <a href="agregarProducto.php"><input type="button" name="btnaAddProduct" value="Agregar Libro"></a>
     <br>
     <br>
     <table>
       <thead>
         <tr>
           <th>Id</th>
           <th>Nombre</th>
           <th>Nombre de usuario</th>
           <th>Contraseña</th>
           <th>Email</th>
         </tr>
       </thead>
       <tbody>
         <?php
          foreach ($matrizUsuarios as $registro) {
            ?>
            <tr>
              <td><?php echo $registro['id']; ?></td>
              <td><?php echo $registro['name']; ?></td>
              <td><?php echo $registro['user_name']; ?></td>
              <td><?php echo $registro['password']; ?></td>
              <td><?php echo $registro['email']; ?></td>
              <?php
                echo '<td><a href="usuarioEliminar.php?idUsuario='.$registro["id"].'">Eliminar</a></td>';
                echo '<td><a href="edidarUsuario.php?idUsuario='.$registro["id"].'">Modificar</a></td>';
               ?>
            </tr>
            <?php
          }
          ?>
       </tbody>
     </table>
     <a href="agregarUsuario.php"> <input type="button" name="btnaAddUser" value="Agregar usuario"> </a>
   </body>
 </html>
